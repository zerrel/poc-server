$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("classpath:feature/AuthorController.feature");
formatter.feature({
  "name": "Author functionality",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "Getting author by id",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "an author with id 17",
  "keyword": "Given "
});
formatter.match({
  "location": "MyStepdefs.an_author_with_id(Integer)"
});
formatter.result({
  "error_message": "cucumber.api.PendingException: TODO: implement me\n\tat steps.MyStepdefs.an_author_with_id(MyStepdefs.java:16)\n\tat ✽.an author with id 17(classpath:feature/AuthorController.feature:3)\n",
  "status": "pending"
});
formatter.step({
  "name": "I make a GET request to \"/author/search/id\u003d17\"",
  "keyword": "When "
});
formatter.match({
  "location": "MyStepdefs.i_make_a_GET_request_to(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I receive an author with id 17",
  "keyword": "Then "
});
formatter.match({
  "location": "MyStepdefs.i_receive_an_author_with_id(Integer)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "I receive a status code of 200",
  "keyword": "And "
});
formatter.match({
  "location": "MyStepdefs.i_receive_a_status_code_of(Integer)"
});
formatter.result({
  "status": "skipped"
});
});