Feature: Retrieve citation by id
  Scenario Outline: client makes call to GET /citation/search/id=<id>
    When the client calls /citation/search/id={id}
    Then the client receives status code of 200
    And the client receives a citation with id <id>

  Examples:
    | id  |
    | 1   |
    | 2   |
    | 3   |
    | 4   |