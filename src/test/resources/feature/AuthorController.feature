Feature: Author functionality
  Scenario: Getting author by id
    Given an author with id 17
    When I GET an author with id 17
    Then I receive an author with id 17
    And I receive a status code of 200