package com.eloy.user;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.persistence.EntityNotFoundException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {
    ArgumentCaptor<User> captor = ArgumentCaptor.forClass(User.class);

    @Mock
    private UserRepository userRepository;

    @Mock
    private UserMapper userMapper;

    @InjectMocks
    private UserService testSubject;

    @Before
    public void setup() {
    }

    @After
    public void after() {
        verifyNoMoreInteractions(userRepository);
    }

    @Test
    public void findAll_success() {

        testSubject.findAll();

        verify(userRepository).findAll();
    }

    @Test
    public void findAll_failure() {
        when(userRepository.findAll()).thenThrow(EntityNotFoundException.class);

        try {
            testSubject.findAll();
        } catch (EntityNotFoundException e) {
            verify(userRepository).findAll();
        }
    }

    @Test
    public void findById_success() {

        testSubject.findById(anyLong());

        // verify getOne method of repository is called
        verify(userRepository).getOne(anyLong());
    }

    @Test
    public void findById_failure() {

        when(userRepository.getOne(1L)).thenThrow(new EntityNotFoundException());

        // verify EntityNotFoundException is thrown
        try {
            testSubject.findById(1L);
            fail();
        } catch (EntityNotFoundException e) {
            verify(userRepository).getOne(1L);
        }
    }

    @Test
    public void findByUsername_success() {
        testSubject.findByUsername(anyString());

        //verify findByFirstName method of repository is called
        verify(userRepository).findByUsername(anyString());
    }

    @Test
    public void findByFirstName_failure() {

        when(userRepository.findByUsername(anyString())).thenThrow(new EntityNotFoundException());

        // verify EntityNotFoundException is thrown
        try {
            testSubject.findByUsername(anyString());
            fail();
        } catch (EntityNotFoundException e) {
            verify(userRepository).findByUsername(anyString());
        }
    }

    @Test
    public void create_success() {

        User user = new User().id(1L).username("aaa").password("bbb");
        UserDto userDto = new UserDto().id(1L).username("aaa");

        // should create user and convert to DTO
        when(userRepository.save(any(User.class))).thenReturn(user);
        when(userMapper.toEntity(any(UserDto.class))).thenReturn(user);

        testSubject.create(userDto);

        // verify save method of repository is called
        verify(userRepository).save(user);
    }

    @Test
    public void create_failure() {

        User user = new User().id(1L).username("aaa").password("bbb");
        UserDto userDto = new UserDto().id(1L).username("aaa");

        when(userRepository.save(any(User.class))).thenThrow(EntityNotFoundException.class);
        when(userMapper.toEntity(any(UserDto.class))).thenReturn(user);


        try {
            testSubject.create(userDto);
            fail();
        } catch (EntityNotFoundException e) {
            verify(userRepository).save(user);
        }
    }

    @Test
    public void delete_success() {

        Long id = 1L;

        // mock user for logging purposes
        User user = new User().id(id).username("aaa").password("bbb");
        when(userRepository.getOne(id)).thenReturn(user);

        testSubject.delete(id);

        // should fetch the user then delete it
        verify(userRepository).getOne(id);
        verify(userRepository).delete(captor.capture());

        // assert that the id remains the same as the request
        assertEquals(id, captor.getValue().getId());
    }

    @Test
    public void delete_failure() {

        Long id = 1L;
        User user = new User().id(id).username("aaa").password("bbb");
        when(userRepository.getOne(id)).thenReturn(user);
        doThrow(EntityNotFoundException.class).when(userRepository).delete(user);

        try {
            testSubject.delete(id);
            fail();
        } catch (EntityNotFoundException e) {
            // should fetch the user then delete it
            verify(userRepository).getOne(id);
            verify(userRepository).delete(user);
        }
    }
}