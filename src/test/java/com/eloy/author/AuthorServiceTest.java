package com.eloy.author;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.persistence.EntityNotFoundException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AuthorServiceTest {

    ArgumentCaptor<Author> captor = ArgumentCaptor.forClass(Author.class);

    @Mock
    private AuthorRepository authorRepository;

    @Mock
    private AuthorMapper authorMapper;

    @InjectMocks
    private AuthorService testSubject;

    @Before
    public void setup() {
    }

    @After
    public void after() {
        verifyNoMoreInteractions(authorRepository);
    }

    @Test
    public void findAll_success() {
        testSubject.findAll();

        verify(authorRepository).findAll();
    }

    @Test
    public void findAll_failure() {
        when(authorRepository.findAll()).thenThrow(EntityNotFoundException.class);

        try {
            testSubject.findAll();
            fail();
        } catch (EntityNotFoundException e) {
            verify(authorRepository).findAll();
        }
    }

    @Test
    public void findById_success() {
        testSubject.findById(anyLong());

        // verify getOne method of repository is called
        verify(authorRepository).getOne(anyLong());
    }

    @Test
    public void findById_failure() {
        when(authorRepository.getOne(1L)).thenThrow(new EntityNotFoundException());

        // verify EntityNotFoundException is thrown
        try {
            testSubject.findById(1L);
            fail();
        } catch (EntityNotFoundException e) {
            verify(authorRepository).getOne(1L);
        }
    }

    @Test
    public void findByFirstName_success() {
        testSubject.findByFirstName(anyString());

        //verify findByFirstName method of repository is called
        verify(authorRepository).findByFirstName(anyString());
    }

    @Test
    public void findByFirstName_failure() {
        when(authorRepository.findByFirstName(anyString())).thenThrow(new EntityNotFoundException());

        // verify EntityNotFoundException is thrown
        try {
            testSubject.findByFirstName(anyString());
            fail();
        } catch (EntityNotFoundException e) {
            verify(authorRepository).findByFirstName(anyString());
        }
    }

    @Test
    public void findByLastName_success() {
        testSubject.findByLastName(anyString());

        //verify findByLastName method of repository is called
        verify(authorRepository).findByLastName(anyString());
    }

    @Test
    public void findByLastName_failure() {
        when(authorRepository.findByLastName(anyString())).thenThrow(new EntityNotFoundException());

        // verify EntityNotFoundException is thrown
        try {
            testSubject.findByLastName(anyString());
            fail();
        } catch (EntityNotFoundException e) {
            verify(authorRepository).findByLastName(anyString());
        }
    }

    @Test
    public void findByInitials_success() {
        testSubject.findByInitials(anyString());

        //verify findByInitials method of repository is called
        verify(authorRepository).findByInitials(anyString());
    }

    @Test
    public void findByInitials_failure() {
        when(authorRepository.findByInitials(anyString())).thenThrow(new EntityNotFoundException());

        // verify EntityNotFoundException is thrown
        try {
            testSubject.findByInitials(anyString());
            fail();
        } catch (EntityNotFoundException e) {
            verify(authorRepository).findByInitials(anyString());
        }
    }

    @Test
    public void create_success() {
        Author author = new Author().id(1L).firstName("aaa").lastName("bbb");
        AuthorDto authorDto = new AuthorDto().id(1L).firstName("aaa").lastName("bbb");

        // should create author and convert to DTO
        when(authorRepository.save(any(Author.class))).thenReturn(author);
        when(authorMapper.toEntity(any(AuthorDto.class))).thenReturn(author);

        testSubject.create(authorDto);

        // verify save method of repository is called
        verify(authorRepository).save(author);
    }

    @Test
    public void create_failure() {
        Author author = new Author().id(1L).firstName("aaa").lastName("bbb");
        AuthorDto authorDto = new AuthorDto().id(1L).firstName("aaa").lastName("bbb");

        when(authorRepository.save(any(Author.class))).thenThrow(EntityNotFoundException.class);
        when(authorMapper.toEntity(any(AuthorDto.class))).thenReturn(author);


        try {
            testSubject.create(authorDto);
            fail();
        } catch (EntityNotFoundException e) {
            verify(authorRepository).save(author);
        }
    }

    @Test
    public void delete_success() {
        Long id = 1L;

        // mock author for logging purposes
        Author author = new Author().id(id).firstName("aaa").lastName("bbb");
        when(authorRepository.getOne(id)).thenReturn(author);

        testSubject.delete(id);

        // should fetch the author then delete it
        verify(authorRepository).getOne(id);
        verify(authorRepository).delete(captor.capture());

        // assert that the id remains the same as the request
        assertEquals(id, captor.getValue().getId());
    }

    @Test
    public void delete_failure() {

        Long id = 1L;
        Author author = new Author().id(id).firstName("aaa").lastName("bbb");
        when(authorRepository.getOne(id)).thenReturn(author);
        doThrow(EntityNotFoundException.class).when(authorRepository).delete(author);

        try {
            testSubject.delete(id);
            fail();
        } catch (EntityNotFoundException e) {
            // should fetch the author then delete it
            verify(authorRepository).getOne(id);
            verify(authorRepository).delete(author);
        }
    }
}