package com.eloy.literature;

import com.eloy.author.Author;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.anyInt;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class LiteratureServiceTest {

    ArgumentCaptor<Literature> captor = ArgumentCaptor.forClass(Literature.class);

    @Mock
    private LiteratureRepository literatureRepository;

    @Mock
    private LiteratureMapper literatureMapper;

    @InjectMocks
    private LiteratureService testSubject;

    @Before
    public void setup() {
    }

    @After
    public void after() {
        verifyNoMoreInteractions(literatureRepository);
    }

    @Test
    public void findAll_success() {

        testSubject.findAll();

        verify(literatureRepository).findAll();
    }

    @Test
    public void findAll_failure() {
        when(literatureRepository.findAll()).thenThrow(EntityNotFoundException.class);

        try {
            testSubject.findAll();
            fail();
        } catch (EntityNotFoundException e) {
            verify(literatureRepository).findAll();
        }
    }

    @Test
    public void findById_success() {

        testSubject.findById(anyLong());

        // verify getOne method of repository is called
        verify(literatureRepository).getOne(anyLong());
    }

    @Test
    public void findById_failure() {

        when(literatureRepository.getOne(1L)).thenThrow(new EntityNotFoundException());

        // verify EntityNotFoundException is thrown
        try {
            testSubject.findById(1L);
            fail();
        } catch (EntityNotFoundException e) {
            verify(literatureRepository).getOne(1L);
        }
    }

    @Test
    public void findByTitle_success() {

        testSubject.findByTitle(anyString());

        //verify findByFirstName method of repository is called
        verify(literatureRepository).findAllByTitle(anyString());
    }

    @Test
    public void findByTitle_failure() {

        when(literatureRepository.findAllByTitle(anyString())).thenThrow(new EntityNotFoundException());

        // verify EntityNotFoundException is thrown
        try {
            testSubject.findByTitle(anyString());
            fail();
        } catch (EntityNotFoundException e) {
            verify(literatureRepository).findAllByTitle(anyString());
        }
    }

    @Test
    public void findByAuthors_success() {
        testSubject.findByAuthor(anyLong());

        //verify findByAuthor method of repository is called
        verify(literatureRepository).findByAuthors_id(anyLong());
    }

    @Test
    public void findByAuthors_failure() {
        when(literatureRepository.findByAuthors_id(anyLong())).thenThrow(EntityNotFoundException.class);

        // verify EntityNotFoundException is thrown
        try {
            testSubject.findByAuthor(anyLong());
            fail();
        } catch (EntityNotFoundException e) {
            verify(literatureRepository).findByAuthors_id(anyLong());
        }
    }

    @Test
    public void findByIsbn_success() {
        testSubject.findByIsbn(anyString());

        //verify findByIsbn method of repository is called
        verify(literatureRepository).findByIsbn(anyString());
    }

    @Test
    public void findByIsbn_failure() {
        when(literatureRepository.findByIsbn(anyString())).thenThrow(EntityNotFoundException.class);

        // verify EntityNotFoundException is thrown
        try {
            testSubject.findByIsbn(anyString());
            fail();
        } catch (EntityNotFoundException e) {
            verify(literatureRepository).findByIsbn(anyString());
        }
    }

    @Test
    public void findByPoi_success() {
        testSubject.findByPoi(anyString());

        //verify findByPoi method of repository is called
        verify(literatureRepository).findByPoi(anyString());
    }

    @Test
    public void findByPoi_failure() {
        when(literatureRepository.findByPoi(anyString())).thenThrow(EntityNotFoundException.class);

        // verify EntityNotFoundException is thrown
        try {
            testSubject.findByPoi(anyString());
            fail();
        } catch (EntityNotFoundException e) {
            verify(literatureRepository).findByPoi(anyString());
        }
    }

    @Test
    public void findByDatePublished_success() {
        LocalDate date = LocalDate.now();
        testSubject.findByDatePublished(date.toString());

        //verify findByDatePublished method of repository is called
        verify(literatureRepository).findAllByDatePublished(any(LocalDate.class));
    }

    @Test
    public void findByDatePublished_failure() {
        LocalDate date = LocalDate.now();
        when(literatureRepository.findAllByDatePublished(any(LocalDate.class))).thenThrow(EntityNotFoundException.class);

        // verify EntityNotFoundException is thrown
        try {
            testSubject.findByDatePublished(date.toString());
            fail();
        } catch (EntityNotFoundException e) {
            verify(literatureRepository).findAllByDatePublished(any(LocalDate.class));
        }
    }

    @Test
    public void findByUrl_success() {
        testSubject.findByUrl(anyString());

        // verify findByUrl method of repository is called
        verify(literatureRepository).findByUrl(anyString());
    }

    @Test
    public void findByUrl_failure() {
        when(literatureRepository.findByUrl(anyString())).thenThrow(EntityNotFoundException.class);

        // verify EntityNotFoundException is thrown
        try {
            testSubject.findByUrl(anyString());
            fail();
        } catch (EntityNotFoundException e) {
            verify(literatureRepository).findByUrl(anyString());
        }
    }

    @Test
    public void findByPeerReviewed_success() {
        testSubject.findByPeerReviewed(anyInt());

        //verify findByPeerReviewed method of repository is called
        verify(literatureRepository).findAllByPeerReviewed(anyInt());
    }

    @Test
    public void findByPeerReviewed_failure() {
        when(literatureRepository.findAllByPeerReviewed(anyInt())).thenThrow(EntityNotFoundException.class);

        // verify EntityNotFoundException is thrown
        try {
            testSubject.findByPeerReviewed(anyInt());
            fail();
        } catch (EntityNotFoundException e) {
            verify(literatureRepository).findAllByPeerReviewed(anyInt());
        }
    }

    @Test
    public void create_success() {
        Long id = 1L;

        List<Author> authors = new ArrayList<>();
        authors.add(new Author().firstName("aaa").lastName("bbb"));
        // mock literature for logging purposes
        Literature literature = new Literature().id(id).title("aaa").authors(authors).isbn("111").poi("222").peerReviewed(0);

        // should create literature and convert to DTO
        when(literatureRepository.save(any(Literature.class))).thenReturn(literature);
        literatureRepository.save(literature);

        // verify save method of repository is called
        verify(literatureRepository).save(literature);
    }

    @Test
    public void create_failure() {
        Long id = 1L;

        List<Author> authors = new ArrayList<>();
        authors.add(new Author().firstName("aaa").lastName("bbb"));
        List<Long> authorIds = new ArrayList<>();
        authorIds.add(id);
        // mock literature for logging purposes
        Literature literature = new Literature().id(id).title("aaa").authors(authors).isbn("111").poi("222").peerReviewed(0);
        LiteratureDto literatureDto = new LiteratureDto().id(id).title("aaa").authors(authorIds).isbn("111").poi("222").peerReviewed(0);

        when(literatureRepository.save(any(Literature.class))).thenThrow(EntityNotFoundException.class);
        when(literatureMapper.toEntity(any(LiteratureDto.class))).thenReturn(literature);

        // verify EntityNotFoundException is thrown
        try {
            testSubject.create(literatureDto);
            fail();
        } catch (EntityNotFoundException e) {
            verify(literatureRepository).save(literature);
        }
    }

    @Test
    public void update_success() {
        Long id = 1L;

        List<Author> authors = new ArrayList<>();
        authors.add(new Author().firstName("aaa").lastName("bbb"));

        Literature literature = new Literature().id(id).title("aaa").authors(authors).isbn("111").poi("222").peerReviewed(0);
    }

    @Test
    public void update_failure() {

    }

    @Test
    public void delete_success() {
        Long id = 1L;

        List<Author> authors = new ArrayList<>();
        authors.add(new Author().firstName("aaa").lastName("bbb"));
        // mock literature for logging purposes
        Literature literature = new Literature().id(id).title("aaa").authors(authors).isbn("111").poi("222").peerReviewed(0);
        when(literatureRepository.getOne(id)).thenReturn(literature);

        testSubject.delete(id);

        // should fetch the literature then delete it
        verify(literatureRepository).getOne(id);
        verify(literatureRepository).delete(captor.capture());

        // assert that the id remains the same as the request
        assertEquals(id, captor.getValue().getId());
    }

    @Test
    public void delete_failure() {

        Long id = 1L;
        List<Author> authors = new ArrayList<>();
        authors.add(new Author().firstName("aaa").lastName("bbb"));
        // mock literature for logging purposes
        Literature literature = new Literature().id(id).title("aaa").authors(authors).isbn("111").poi("222").peerReviewed(0);
        when(literatureRepository.getOne(id)).thenReturn(literature);
        doThrow(EntityNotFoundException.class).when(literatureRepository).delete(literature);

        // verify EntityNotFoundException is thrown
        try {
            testSubject.delete(id);
            fail();
        } catch (EntityNotFoundException e) {
            // should fetch the literature then delete it
            verify(literatureRepository).getOne(id);
            verify(literatureRepository).delete(literature);
        }
    }
}