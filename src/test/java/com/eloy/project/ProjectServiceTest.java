package com.eloy.project;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.persistence.EntityNotFoundException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ProjectServiceTest {

    ArgumentCaptor<Project> captor = ArgumentCaptor.forClass(Project.class);

    @Mock
    private ProjectRepository projectRepository;

    @Mock
    private ProjectMapper projectMapper;

    @InjectMocks
    private ProjectService testSubject;

    @Before
    public void setup() {
    }

    @After
    public void after() {
        verifyNoMoreInteractions(projectRepository);
    }

    @Test
    public void findAll_success() {

        testSubject.findAll();

        verify(projectRepository).findAll();
    }

    @Test
    public void findAll_failure() {
        when(projectRepository.findAll()).thenThrow(EntityNotFoundException.class);

        try {
            testSubject.findAll();
            fail();
        } catch (EntityNotFoundException e) {
            verify(projectRepository).findAll();
        }
    }

    @Test
    public void findById_success() {

        testSubject.findById(anyLong());

        // verify getOne method of repository is called
        verify(projectRepository).getOne(anyLong());
    }

    @Test
    public void findById_failure() {

        when(projectRepository.getOne(1L)).thenThrow(new EntityNotFoundException());

        // verify EntityNotFoundException is thrown
        try {
            testSubject.findById(1L);
            fail();
        } catch (EntityNotFoundException e) {
            verify(projectRepository).getOne(1L);
        }
    }

    @Test
    public void findByName_success() {
        testSubject.findByName(anyString());

        //verify findByFirstName method of repository is called
        verify(projectRepository).findAllByName(anyString());
    }

    @Test
    public void findByName_failure() {

        when(projectRepository.findAllByName(anyString())).thenThrow(new EntityNotFoundException());

        // verify EntityNotFoundException is thrown
        try {
            testSubject.findByName(anyString());
            fail();
        } catch (EntityNotFoundException e) {
            verify(projectRepository).findAllByName(anyString());
        }
    }

    @Test
    public void findByState_success() {
        testSubject.findByState(ProjectState.IN_PROGRESS);

        //verify findByLastName method of repository is called
        verify(projectRepository).findAllByState(any(ProjectState.class));
    }

    @Test
    public void findByState_failure() {

        when(projectRepository.findAllByState(any(ProjectState.class))).thenThrow(new EntityNotFoundException());

        // verify EntityNotFoundException is thrown
        try {
            testSubject.findByState(ProjectState.IN_PROGRESS);
            fail();
        } catch (EntityNotFoundException e) {
            verify(projectRepository).findAllByState(any(ProjectState.class));
        }
    }

    @Test
    public void create_success() {

        Project project = new Project().id(1L).name("aaa").state(ProjectState.IN_PROGRESS);
        ProjectDto projectDto = new ProjectDto().id(1L).name("aaa").state(ProjectState.IN_PROGRESS);

        // should create project and convert to DTO
        when(projectRepository.save(any(Project.class))).thenReturn(project);
        when(projectMapper.toEntity(any(ProjectDto.class))).thenReturn(project);

        testSubject.create(projectDto);

        // verify save method of repository is called
        verify(projectRepository).save(project);
    }

    @Test
    public void create_failure() {

        Project project = new Project().id(1L).name("aaa").state(ProjectState.IN_PROGRESS);
        ProjectDto projectDto = new ProjectDto().id(1L).name("aaa").state(ProjectState.IN_PROGRESS);

        when(projectRepository.save(any(Project.class))).thenThrow(EntityNotFoundException.class);
        when(projectMapper.toEntity(any(ProjectDto.class))).thenReturn(project);


        try {
            testSubject.create(projectDto);
            fail();
        } catch (EntityNotFoundException e) {
            verify(projectRepository).save(project);
        }
    }

    @Test
    public void delete_success() {

        Long id = 1L;

        // mock project for logging purposes
        Project project = new Project().id(id).name("aaa").state(ProjectState.IN_PROGRESS);
        when(projectRepository.getOne(id)).thenReturn(project);

        testSubject.delete(id);

        // should fetch the project then delete it
        verify(projectRepository).getOne(id);
        verify(projectRepository).delete(captor.capture());

        // assert that the id remains the same as the request
        assertEquals(id, captor.getValue().getId());
    }

    @Test
    public void delete_failure() {

        Long id = 1L;
        Project project = new Project().id(id).name("aaa").state(ProjectState.IN_PROGRESS);
        when(projectRepository.getOne(id)).thenReturn(project);
        doThrow(EntityNotFoundException.class).when(projectRepository).delete(project);

        try {
            testSubject.delete(id);
            fail();
        } catch (EntityNotFoundException e) {
            // should fetch the project then delete it
            verify(projectRepository).getOne(id);
            verify(projectRepository).delete(project);
        }
    }

}