package com.eloy.project;

import com.eloy.user.User;
import com.eloy.user.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Service
public class ProjectService {

    private final ProjectRepository projectRepository;
    private final UserRepository userRepository;
    private final ProjectMapper projectMapper;

    private static final Logger logger = LoggerFactory.getLogger(ProjectService.class);

    @Autowired
    public ProjectService(ProjectRepository projectRepository, UserRepository userRepository, ProjectMapper projectMapper) {
        this.projectRepository = projectRepository;
        this.userRepository = userRepository;
        this.projectMapper = projectMapper;
    }


    public List<ProjectDto> findAll() {
        return projectMapper.toDtoList(projectRepository.findAll());
    }

    public ProjectDto findById(Long id) {
        return projectMapper.toDto(projectRepository.getOne(id));
    }

    public List<ProjectDto> findByName(String name) {
        return projectMapper.toDtoList(projectRepository.findAllByName(name));
    }

    public List<ProjectDto> findByState(ProjectState state) {
        return projectMapper.toDtoList(projectRepository.findAllByState(state));
    }

    @Transactional
    public ProjectDto create(ProjectDto dto) {
        List<User> members = new ArrayList<>();
        try {
            for (Long m : dto.getMembers()) {
                members.add(userRepository.getOne(m));
            }
        } catch (NullPointerException e) {
            System.out.println(e.getMessage());
        }
        Project project = projectMapper.toEntity(dto);
        project.setMembers(members);
        return projectMapper.toDto(projectRepository.save(project));
    }

    @Transactional
    public ProjectDto update(Long id, ProjectDto dto) {
        List<User> members = dto.getMembers()
                .stream()
                .map(userRepository::getOne)
                .collect(Collectors.toList());
        Project project = projectMapper.toEntity(dto, id);
        project.members(members);
        Project updatedProject = projectRepository.save(project);
        return projectMapper.toDto(updatedProject);
    }

    @Transactional
    public String delete(Long id) {
        Project project = projectRepository.getOne(id);
        projectRepository.delete(project);
        return project.getName() + " deleted";
    }
}
