package com.eloy.project;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * ProjectDto
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2019-02-28T16:36:17.716+01:00[Europe/Paris]")
public class ProjectDto {
    @JsonProperty("id")
    private Long id = null;

    @JsonProperty("name")
    private String name = null;

    @JsonProperty("state")
    private ProjectState state = null;

    @JsonProperty("subProjects")
    @Valid
    private List<Long> subProjects = null;

    @JsonProperty("members")
    @Valid
    private List<Long> members = null;

    public ProjectDto id(Long id) {
        this.id = id;
        return this;
    }

    /**
     * Get id
     *
     * @return id
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ProjectDto name(String name) {
        this.name = name;
        return this;
    }

    /**
     * Get name
     *
     * @return name
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ProjectDto state(ProjectState state) {
        this.state = state;
        return this;
    }

    /**
     * Get state
     *
     * @return state
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull

    @Valid
    public ProjectState getState() {
        return state;
    }

    public void setState(ProjectState state) {
        this.state = state;
    }

    public ProjectDto subProjects(List<Long> subProjects) {
        this.subProjects = subProjects;
        return this;
    }

    public ProjectDto addSubProjectsItem(Long subProjectsItem) {
        if (this.subProjects == null) {
            this.subProjects = new ArrayList<Long>();
        }
        this.subProjects.add(subProjectsItem);
        return this;
    }

    /**
     * Get subProjects
     *
     * @return subProjects
     **/
    @ApiModelProperty(value = "")

    public List<Long> getSubProjects() {
        return subProjects;
    }

    public void setSubProjects(List<Long> subProjects) {
        this.subProjects = subProjects;
    }

    public ProjectDto members(List<Long> members) {
        this.members = members;
        return this;
    }

    public ProjectDto addMembersItem(Long membersItem) {
        if (this.members == null) {
            this.members = new ArrayList<Long>();
        }
        this.members.add(membersItem);
        return this;
    }

    /**
     * Get members
     *
     * @return members
     **/
    @ApiModelProperty(value = "")

    public List<Long> getMembers() {
        return members;
    }

    public void setMembers(List<Long> members) {
        this.members = members;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ProjectDto project = (ProjectDto) o;
        return Objects.equals(this.id, project.id) &&
                Objects.equals(this.name, project.name) &&
                Objects.equals(this.state, project.state) &&
                Objects.equals(this.subProjects, project.subProjects) &&
                Objects.equals(this.members, project.members);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, state, subProjects, members);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class ProjectDto {\n");

        sb.append("    id: ").append(toIndentedString(id)).append("\n");
        sb.append("    name: ").append(toIndentedString(name)).append("\n");
        sb.append("    state: ").append(toIndentedString(state)).append("\n");
        sb.append("    subProjects: ").append(toIndentedString(subProjects)).append("\n");
        sb.append("    members: ").append(toIndentedString(members)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
