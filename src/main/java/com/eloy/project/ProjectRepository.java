package com.eloy.project;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Long> {

//    @Query("select p from ProjectDto where p.name = :name")
//    ProjectDto findByUsername(@Param("name") String name);

    List<Project> findAllByName(String name);

//    @Query("select p from ProjectDto where p.state = :state")
//    ProjectDto findByState(@Param("state") ProjectDto.State state);

    List<Project> findAllByState(ProjectState state);
}
