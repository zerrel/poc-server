package com.eloy.project;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.eloy.user.User;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.List;

@Entity
@DynamicUpdate
@JsonIgnoreProperties(ignoreUnknown = true)
public class Project {

    /**
     * Properties
     */

    @Id
    @Column(name = "project_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "project_name")
    private String name;

    @Column(name = "project_state")
    @Enumerated(EnumType.STRING)
    private ProjectState state;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "mainProject", referencedColumnName = "project_id")
    private Project mainProject;

    @OneToMany(mappedBy = "mainProject", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Project> subProjects;

    @ManyToMany(targetEntity = User.class)
    @JoinTable(
            name = "USER_PROJECT",
            joinColumns = @JoinColumn(name = "PROJECT_ID"),
            inverseJoinColumns = @JoinColumn(name = "USER_ID")
    )
    private List<User> members;

    /**
     * Fluent API for method chaining
     */

    public Project id(Long id) {
        this.id = id;
        return this;
    }

    public Project name(String name) {
        this.name = name;
        return this;
    }

    public Project state(ProjectState state) {
        this.state = state;
        return this;
    }

    public Project mainProject(Project mainProject) {
        this.mainProject = mainProject;
        return this;
    }

    public Project subProjects(List<Project> subProjects) {
        this.subProjects = subProjects;
        return this;
    }

    public Project members(List<User> members) {
        this.members = members;
        return this;
    }

    /**
     * Getters and Setters
     */

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ProjectState getState() {
        return state;
    }

    public void setState(ProjectState state) {
        this.state = state;
    }

    public Project getMainProject() {
        return mainProject;
    }

    public void setMainProject(Project mainProject) {
        this.mainProject = mainProject;
    }

    public List<Project> getSubProjects() {
        return subProjects;
    }

    public void setSubProjects(List<Project> subProjects) {
        this.subProjects = subProjects;
    }

    public List<User> getMembers() {
        return members;
    }

    public void setMembers(List<User> members) {
        this.members = members;
    }
}


