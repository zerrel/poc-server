package com.eloy.project;

import com.eloy.user.User;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "Spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public abstract class ProjectMapper {

    @Mapping(target = "members", ignore = true)
    @Mapping(target = "subProjects", ignore = true)
    public abstract Project toEntity(ProjectDto dto);

    @Mapping(target = "id", source = "projectId")
    @Mapping(target = "subProjects", ignore = true)
    public abstract Project toEntity(ProjectDto dto, Long projectId);

    User toEntity(Long member) {
        return new User().id(member);
    }

    @IterableMapping(nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT)
    public abstract List<Project> toEntityList(List<ProjectDto> dtoList);

    @Mapping(target = "subProjects", ignore = true)
    public abstract ProjectDto toDto(Project project);

    Long toDto(User member) {
        return member.getId();
    }

    @IterableMapping(nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT)
    public abstract List<ProjectDto> toDtoList(List<Project> projectList);
}
