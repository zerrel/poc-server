package com.eloy.project;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/project")
public class ProjectController {

    private final ProjectService projectService;

    @Autowired
    public ProjectController(ProjectService projectservice) {
        this.projectService = projectservice;
    }

    @GetMapping("/search/all")
    public ResponseEntity<List<ProjectDto>> findAllProjects() {
        return new ResponseEntity<>(projectService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/search/id={id}")
    public ResponseEntity<ProjectDto> findById(@PathVariable(value = "id") Long id) {
        return new ResponseEntity<>(projectService.findById(id), HttpStatus.OK);
    }

    @GetMapping("/search/name={name}")
    public ResponseEntity<List<ProjectDto>> findByName(@PathVariable(value = "name") String name) {
        return new ResponseEntity<>(projectService.findByName(name), HttpStatus.OK);
    }

    @GetMapping("/search/state={state}")
    public ResponseEntity<List<ProjectDto>> findByState(@PathVariable(value = "state") ProjectState state) {
        return new ResponseEntity<>(projectService.findByState(state), HttpStatus.OK);
    }

    @PostMapping("/new")
    public ResponseEntity<ProjectDto> createProject(@RequestBody ProjectDto project) {
        return new ResponseEntity<>(projectService.create(project), HttpStatus.OK);
    }

    @PatchMapping("/edit/id={id}")
    public ResponseEntity<ProjectDto> updateProject(@PathVariable(value = "id") Long id, @RequestBody ProjectDto project) {
        return new ResponseEntity<>(projectService.update(id, project), HttpStatus.OK);
    }

    @RequestMapping("/delete/id={id}")
    public ResponseEntity<String> deleteProject(@PathVariable(value = "id") Long id) {
        return new ResponseEntity<>(projectService.delete(id), HttpStatus.OK);
    }
}
