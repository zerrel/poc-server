package com.eloy.literature;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface LiteratureRepository extends JpaRepository<Literature, Long> {

    List<Literature> findAllByTitle(String title);

    List<Literature> findByAuthors_id(Long id);

    Literature findByPoi(String poi);

    Literature findByIsbn(String isbn);

    List<Literature> findAllByDatePublished(LocalDate datePublished);

    Literature findByUrl(String url);

    List<Literature> findAllByPeerReviewed(Integer peerReviewed);
}
