package com.eloy.literature;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin("*")
@RestController
@RequestMapping("/literature")
public class LiteratureController {

    private final LiteratureService literatureService;

    Logger logger = LoggerFactory.getLogger(LiteratureController.class);


    @Autowired
    public LiteratureController(LiteratureService literatureService) {
        this.literatureService = literatureService;
    }


    @GetMapping("/search/all")
    public ResponseEntity<List<LiteratureDto>> findAllLiterature() {
        return new ResponseEntity<>(
                literatureService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/search/id={id}")
    public ResponseEntity<LiteratureDto> findById(@PathVariable(value = "id") Long id) {
        return new ResponseEntity<>(literatureService.findById(id), HttpStatus.OK);
    }

    @GetMapping("/search/title={title}")
    public ResponseEntity<List<LiteratureDto>> findByTitle(@PathVariable(value = "title") String title) {
        return new ResponseEntity<>(literatureService.findByTitle(title), HttpStatus.OK);
    }

    @GetMapping("/search/author={author}")
    public ResponseEntity<List<LiteratureDto>> findByAuthor(@PathVariable(value = "author") Long id) {
        return new ResponseEntity<>(literatureService.findByAuthor(id), HttpStatus.OK);
    }

    @GetMapping("/search/poi={poi}")
    public ResponseEntity<LiteratureDto> findByPoi(@PathVariable(value = "poi") String poi) {
        return new ResponseEntity<>(literatureService.findByPoi(poi), HttpStatus.OK);
    }

    @GetMapping("/search/isbn={isbn}")
    public ResponseEntity<LiteratureDto> findByIsbn(@PathVariable(value = "isbn") String isbn) {
        return new ResponseEntity<>(literatureService.findByIsbn(isbn), HttpStatus.OK);
    }

    @GetMapping("/search/date-published={date}")
    public ResponseEntity<List<LiteratureDto>> findByDatePublished(@PathVariable(value = "date") String date) {
        return new ResponseEntity<>(literatureService.findByDatePublished(date), HttpStatus.OK);
    }

    @GetMapping("/search/url={url}")
    public ResponseEntity<LiteratureDto> findByUrl(@PathVariable(value = "url") String url) {
        return new ResponseEntity<>(literatureService.findByUrl(url), HttpStatus.OK);
    }

    @GetMapping("/search/peer-reviewed={peer-reviewed}")
    public ResponseEntity<List<LiteratureDto>> findByPeerReviewed(@PathVariable(value = "peer-reviewed") Integer peerReviewed) {
        return new ResponseEntity<>(literatureService.findByPeerReviewed(peerReviewed), HttpStatus.OK);
    }

    @PostMapping("/new")
    public ResponseEntity<LiteratureDto> create(@RequestBody LiteratureDto literature) {
        return new ResponseEntity<>(literatureService.create(literature), HttpStatus.OK);
    }

    @PatchMapping("/edit/id={id}")
    public ResponseEntity<LiteratureDto> update(@PathVariable(value = "id") Long id, @RequestBody LiteratureDto literature) {
        logger.warn("!!!!!!!!!#$%#$ˆˆ%&#%$&%ˆˆ*$%&*TY&RUI%TYU");
        return new ResponseEntity<>(literatureService.update(id, literature), HttpStatus.OK);

    }

    @RequestMapping("/delete/id={id}")
    public ResponseEntity<String> delete(@PathVariable(value = "id") Long id) {
        return new ResponseEntity<>(literatureService.delete(id), HttpStatus.OK);
    }
}
