package com.eloy.literature;

import com.eloy.author.Author;
import com.eloy.author.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class LiteratureService {

    private final LiteratureRepository literatureRepository;
    private final AuthorRepository authorRepository;
    private final LiteratureMapper literatureMapper;

    @Autowired
    public LiteratureService(LiteratureRepository LiteratureRepository, AuthorRepository authorRepository, LiteratureMapper literatureMapper) {
        this.literatureRepository = LiteratureRepository;
        this.authorRepository = authorRepository;
        this.literatureMapper = literatureMapper;

    }

    public List<LiteratureDto> findAll() {
        return literatureMapper.toDtoList(literatureRepository.findAll());
    }

    public LiteratureDto findById(Long id) {
        return literatureMapper.toDto(literatureRepository.getOne(id));
    }

    public List<LiteratureDto> findByTitle(String title) {
        return literatureMapper.toDtoList(literatureRepository.findAllByTitle(title));
    }

    public List<LiteratureDto> findByAuthor(Long id) {
        return literatureMapper.toDtoList(literatureRepository.findByAuthors_id(id));
    }

    public LiteratureDto findByPoi(String poi) {
        return literatureMapper.toDto(literatureRepository.findByPoi(poi));
    }

    public LiteratureDto findByIsbn(String isbn) {
        return literatureMapper.toDto(literatureRepository.findByIsbn(isbn));
    }

    public List<LiteratureDto> findByDatePublished(String date) {
        return literatureMapper.toDtoList(literatureRepository.findAllByDatePublished(LocalDate.parse(date)));
    }

    public LiteratureDto findByUrl(String url) {
        return literatureMapper.toDto(literatureRepository.findByUrl(url));
    }

    public List<LiteratureDto> findByPeerReviewed(Integer peerReviewed) {
        return literatureMapper.toDtoList(literatureRepository.findAllByPeerReviewed(peerReviewed));
    }

    public LiteratureDto create(LiteratureDto dto) {
        Literature literature = literatureMapper.toEntity(dto);
        Literature updatedLiterature = literatureRepository.save(literature);
        return literatureMapper.toDto(updatedLiterature);
    }

    public LiteratureDto update(Long id, LiteratureDto literatureDto) {
        List<Author> authors = literatureDto.getAuthors()
                .stream()
                .map(authorRepository::getOne)
                .collect(Collectors.toList());
        Literature literatureEntity = literatureMapper.toEntity(literatureDto, id);
        literatureEntity.authors(authors);
        Literature save = literatureRepository.save(literatureEntity);
        return literatureMapper.toDto(save);
    }

    public String delete(Long id) {
        Literature l = literatureRepository.getOne(id);
        literatureRepository.delete(l);
        return l.getTitle() + " has been deleted.";
    }
}
