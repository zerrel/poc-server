package com.eloy.literature;

import com.eloy.author.Author;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@DynamicUpdate
@JsonIgnoreProperties(ignoreUnknown = true)
public class Literature {

    /**
     * Properties
     */

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;

    @ManyToMany(targetEntity = Author.class, cascade = CascadeType.MERGE)
    @JoinTable(
            name = "AUTHOR_LITERATURE",
            joinColumns = @JoinColumn(name = "LITERATURE_ID"),
            inverseJoinColumns = @JoinColumn(name = "AUTHOR_ID")
    )
    private List<Author> authors;

    @Column(name = "literature_poi", unique = true)
    private String poi;

    @Column(name = "literature_isbn", unique = true)
    private String isbn;

    @Column(name = "literature_date_published")
    private LocalDate datePublished;


    @Column(name = "literature_url", unique = true)
    private String url;

    @Column(name = "literature_peer_reviewed")
    private Integer peerReviewed = -1;

    /**
     * Fluent API for method chaining
     */

    public Literature id(Long id) {
        this.id = id;
        return this;
    }

    public Literature title(String title) {
        this.title = title;
        return this;
    }

    public Literature authors(List<Author> authors) {
        this.authors = authors;
        return this;
    }

    public Literature poi(String poi) {
        this.poi = poi;
        return this;
    }

    public Literature isbn(String isbn) {
        this.isbn = isbn;
        return this;
    }

    public Literature datePublished(LocalDate datePublished) {
        this.datePublished = datePublished;
        return this;
    }

    public Literature url(String url) {
        this.url = url;
        return this;
    }

    public Literature peerReviewed(Integer peerReviewed) {
        this.peerReviewed = peerReviewed;
        return this;
    }

    /**
     * Getters and Setters
     */

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }

    public String getPoi() {
        return poi;
    }

    public void setPoi(String poi) {
        this.poi = poi;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public LocalDate getDatePublished() {
        return datePublished;
    }

    public void setDatePublished(LocalDate datePublished) {
        this.datePublished = datePublished;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getPeerReviewed() {
        return peerReviewed;
    }

    public void setPeerReviewed(Integer peerReviewed) {
        this.peerReviewed = peerReviewed;
    }
}
