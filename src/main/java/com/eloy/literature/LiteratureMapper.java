package com.eloy.literature;

import com.eloy.author.Author;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "Spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public abstract class LiteratureMapper {

    @Mapping(target = "authors", ignore = true)
    public abstract Literature toEntity(LiteratureDto dto);

    @Mapping(target = "id", source = "literatureId")
    public abstract Literature toEntity(LiteratureDto dto, Long literatureId);

    Author toEntity(Long authorId) {
        return new Author().id(authorId);
    }

    @IterableMapping(nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT)
    public abstract List<Literature> toEntityList(List<LiteratureDto> dtoList);

    public abstract LiteratureDto toDto(Literature literature);

    Long toDto(Author author) {
        return author.getId();
    }

    @IterableMapping(nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT)
    public abstract List<LiteratureDto> toDtoList(List<Literature> literatureList);

}
