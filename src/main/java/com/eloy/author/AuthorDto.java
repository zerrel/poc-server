package com.eloy.author;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * AuthorDto
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2019-02-28T16:36:17.716+01:00[Europe/Paris]")
public class AuthorDto {
    @JsonProperty("id")
    private Long id = null;

    @JsonProperty("firstName")
    private String firstName = null;

    @JsonProperty("lastName")
    private String lastName = null;

    @JsonProperty("initials")
    private String initials = null;

    @JsonProperty("literature")
    @Valid
    private List<Long> literature = null;

    public AuthorDto id(Long id) {
        this.id = id;
        return this;
    }

    /**
     * Get id
     *
     * @return id
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AuthorDto firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    /**
     * Get firstName
     *
     * @return firstName
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull

    @Size(min = 1)
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public AuthorDto lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    /**
     * Get lastName
     *
     * @return lastName
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull

    @Size(min = 1)
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public AuthorDto initials(String initials) {
        this.initials = initials;
        return this;
    }

    /**
     * Get initials
     *
     * @return initials
     **/
    @ApiModelProperty(required = true, value = "")
    @NotNull

    @Pattern(regexp = "^([A-Z]\\.)+$")
    @Size(min = 1)
    public String getInitials() {
        return initials;
    }

    public void setInitials(String initials) {
        this.initials = initials;
    }

    public AuthorDto literature(List<Long> literature) {
        this.literature = literature;
        return this;
    }

    public AuthorDto addLiteratureItem(Long literatureItem) {
        if (this.literature == null) {
            this.literature = new ArrayList<Long>();
        }
        this.literature.add(literatureItem);
        return this;
    }

    /**
     * Get literature
     *
     * @return literature
     **/
    @ApiModelProperty(value = "")

    public List<Long> getLiterature() {
        return literature;
    }

    public void setLiterature(List<Long> literature) {
        this.literature = literature;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AuthorDto author = (AuthorDto) o;
        return Objects.equals(this.id, author.id) &&
                Objects.equals(this.firstName, author.firstName) &&
                Objects.equals(this.lastName, author.lastName) &&
                Objects.equals(this.initials, author.initials) &&
                Objects.equals(this.literature, author.literature);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, lastName, initials, literature);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class AuthorDto {\n");

        sb.append("    id: ").append(toIndentedString(id)).append("\n");
        sb.append("    firstName: ").append(toIndentedString(firstName)).append("\n");
        sb.append("    lastName: ").append(toIndentedString(lastName)).append("\n");
        sb.append("    initials: ").append(toIndentedString(initials)).append("\n");
        sb.append("    literature: ").append(toIndentedString(literature)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
