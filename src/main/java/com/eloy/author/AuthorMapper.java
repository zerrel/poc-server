package com.eloy.author;

import com.eloy.literature.Literature;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.WARN)
public abstract class AuthorMapper {

    @Mapping(target = "literature", ignore = true)
    public abstract Author toEntity(AuthorDto dto);

    @Mapping(target = "id", source = "authorId")
    public abstract Author toEntity(AuthorDto dto, Long authorId);

    Literature toEntity(Long literatureId) {
        return new Literature().id(literatureId);
    }

    @IterableMapping(nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT)
    public abstract List<Author> toEntityList(List<AuthorDto> dtoList);

    public abstract AuthorDto toDto(Author entity);

    Long toDto(Literature literature) {
        return literature.getId();
    }

    @IterableMapping(nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT)
    public abstract List<AuthorDto> toDtoList(List<Author> authorList);
}
