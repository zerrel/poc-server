package com.eloy.author;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin("*")
@RestController
@RequestMapping("/author")
public class AuthorController {

    private final AuthorService authorService;

    @Autowired
    public AuthorController(AuthorService authorService) {
        this.authorService = authorService;
    }

    @GetMapping("/search/all")
    public ResponseEntity<List<AuthorDto>> findAll() {
        return new ResponseEntity<>(authorService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/search/id={id}")
    public ResponseEntity<AuthorDto> findById(@PathVariable(value = "id") Long id) {
        return new ResponseEntity<>(authorService.findById(id), HttpStatus.OK);
    }

    @GetMapping("/search/first-name={first_name}")
    public ResponseEntity<List<AuthorDto>> findByFirstName(@PathVariable(value = "first_name") String fName) {
        return new ResponseEntity<>(authorService.findByFirstName(fName), HttpStatus.OK);
    }

    @GetMapping("/search/last-name={last_name}")
    public ResponseEntity<List<AuthorDto>> findByLastName(@PathVariable(value = "last_name") String lName) {
        return new ResponseEntity<>(authorService.findByLastName(lName), HttpStatus.OK);
    }

    @GetMapping("/search/initials={initials}")
    public ResponseEntity<List<AuthorDto>> findByInitials(@PathVariable(value = "initials") String initials) {
        return new ResponseEntity<>(authorService.findByInitials(initials), HttpStatus.OK);
    }

    @PostMapping("/new")
    public ResponseEntity<AuthorDto> create(@RequestBody AuthorDto author) {
        return new ResponseEntity<>(authorService.create(author), HttpStatus.OK);
    }

    @RequestMapping("/delete/id={id}")
    public ResponseEntity<String> delete(@PathVariable(value = "id") Long id) {
        return new ResponseEntity<>(authorService.delete(id), HttpStatus.OK);
    }

    @PatchMapping("/edit/id={id}")
    public ResponseEntity<AuthorDto> update(@PathVariable(value = "id") Long id, @RequestBody AuthorDto author) {
        return new ResponseEntity<>(authorService.update(id, author), HttpStatus.OK);
    }
}
