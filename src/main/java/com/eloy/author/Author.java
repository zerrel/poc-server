package com.eloy.author;

import com.eloy.literature.Literature;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@DynamicUpdate
@JsonIgnoreProperties(ignoreUnknown = true)
public class Author {

    /*
     * Properties
     */

    @Id
    @Column(name = "author_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "author_first_name")
    private String firstName;

    @Column(name = "author_last_name")
    private String lastName;

    @Column(name = "author_initials")
    private String initials;

    @ManyToMany(
            cascade = CascadeType.MERGE,
            mappedBy = "authors",
            targetEntity = Literature.class
    )
    private List<Literature> literature;

    /*
     * Fluent API for method chaining
     */

    public Author id(Long id) {
        this.id = id;
        return this;
    }

    public Author firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public Author lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public Author initials(String initials) {
        this.initials = initials;
        return this;
    }

    public Author literature(List<Literature> literature) {
        this.literature = literature;
        return this;
    }

    /*
     * Getters and Setters
     */

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getInitials() {
        return initials;
    }

    public void setInitials(String initials) {
        this.initials = initials;
    }

    public List<Literature> getLiterature() {
        return literature;
    }

    public void setLiterature(List<Literature> literature) {
        this.literature = literature;
    }
}
