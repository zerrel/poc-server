package com.eloy.author;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class AuthorService {

    private final AuthorRepository authorRepository;
    private final AuthorMapper authorMapper;

    public AuthorService(AuthorRepository authorRepository, AuthorMapper authorMapper) {
        this.authorRepository = authorRepository;
        this.authorMapper = authorMapper;
    }

    public List<AuthorDto> findAll() {
        return authorMapper.toDtoList(authorRepository.findAll());
    }

    public AuthorDto findById(Long id) {
        return authorMapper.toDto(authorRepository.getOne(id));
    }

    public List<AuthorDto> findByFirstName(String fName) {
        return authorMapper.toDtoList(authorRepository.findByFirstName(fName));
    }

    public List<AuthorDto> findByLastName(String lName) {
        return authorMapper.toDtoList(authorRepository.findByLastName(lName));
    }

    public List<AuthorDto> findByInitials(String initials) {
        return authorMapper.toDtoList(authorRepository.findByInitials(initials));
    }

    public AuthorDto create(AuthorDto dto) {
        Author author = authorMapper.toEntity(dto);
        Author newAuthor = authorRepository.save(author);
        return authorMapper.toDto(newAuthor);
    }

    public AuthorDto update(Long id, AuthorDto authorDto) {
        Author author = authorMapper.toEntity(authorDto, id);
        Author updatedAuthor = authorRepository.save(author);
        return authorMapper.toDto(updatedAuthor);
    }

    public String delete(Long id) {
        Author a = authorRepository.getOne(id);
        authorRepository.delete(a);
        return a.getFirstName() + " " + a.getLastName() + " has been deleted.";
    }
}
