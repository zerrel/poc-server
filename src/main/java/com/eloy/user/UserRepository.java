package com.eloy.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    //    @Query("select u from UserDto where u.username = :username")
//    UserDto findUserByUsername(@Param("username") String username);
    User findByUsername(String username);
}
