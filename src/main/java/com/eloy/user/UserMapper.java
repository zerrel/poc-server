package com.eloy.user;

import com.eloy.project.Project;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "Spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public abstract class UserMapper {

    @Mapping(target = "projects", ignore = true)
    public abstract User toEntity(UserDto dto);

    @Mapping(target = "id", source = "userId")
    public abstract User toEntity(UserDto dto, Long userId);

    Project toEntity(Long projectId) {
        return new Project().id(projectId);
    }

    @IterableMapping(nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT)
    public abstract List<User> toEntityList(List<UserDto> dtoList);

    public abstract UserDto toDto(User User);

    Long toDto(Project project) {
        return project.getId();
    }

    @IterableMapping(nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT)
    public abstract List<UserDto> toDtoList(List<User> UserList);
}
