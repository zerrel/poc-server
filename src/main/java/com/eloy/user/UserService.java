package com.eloy.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.List;

@Component
public class UserService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;

    @Autowired
    public UserService(UserRepository userRepository, UserMapper userMapper) {
        this.userRepository = userRepository;
        this.userMapper = userMapper;
    }

    public List<UserDto> findAll() {
        return userMapper.toDtoList(userRepository.findAll());
    }

    public UserDto findById(Long id) {
        return userMapper.toDto(userRepository.getOne(id));
    }

    public UserDto findByUsername(String username) {
        return userMapper.toDto(userRepository.findByUsername(username));
    }

    @Transactional
    public UserDto create(UserDto user) {
        return userMapper.toDto(userRepository.save(userMapper.toEntity(user)));
    }

    @Transactional
    public UserDto update(Long id, UserDto dto) {
        User user = userMapper.toEntity(dto, id);
        User updatedUser = userRepository.save(user);
        return userMapper.toDto(updatedUser);
    }

    @Transactional
    public String delete(Long id) {
        User u = userRepository.getOne(id);
        userRepository.delete(u);
        return u.getUsername() + " deleted";
    }
}
